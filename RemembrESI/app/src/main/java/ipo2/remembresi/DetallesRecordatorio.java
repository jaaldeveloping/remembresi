package ipo2.remembresi;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

public class DetallesRecordatorio extends AppCompatActivity {
    private TextView txtNombre;
    private TextView txtDescripcion;
    private TextView txtCategoria;
    private TextView txtHora;
    private TextView txtFecha;
    private TextView txtUbicacion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detalles_recordatorio);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Detalles");
        setSupportActionBar(toolbar);

        //Asignamos los layout
        txtNombre=(TextView)findViewById(R.id.txtNombre);
        txtDescripcion=(TextView)findViewById(R.id.txtDescripcion);
        txtCategoria=(TextView)findViewById(R.id.txtCategoria);
        txtHora=(TextView)findViewById(R.id.txtHora);
        txtFecha=(TextView)findViewById(R.id.txtFecha);
        txtUbicacion=(TextView)findViewById(R.id.txtUbicacion);


        //Asignamos los datos
        Bundle bundle=getIntent().getExtras();
        txtNombre.setText(bundle.getString("nombre"));
        Log.i("Service","nombre " + bundle.getString("nombre"));
        txtDescripcion.setText(bundle.getString("descripcion"));
        Log.i("Service","descripcion " + bundle.getString("descripcion"));
        txtCategoria.setText(bundle.getString("categoria"));
        Log.i("Service","categoria " + bundle.getString("categoria"));
        txtHora.setText(bundle.getString("hora"));
        Log.i("Service","hora " + bundle.getString("hora"));
        txtFecha.setText(bundle.getString("fecha"));
        Log.i("Service","fecha " + bundle.getString("fecha"));
        txtUbicacion.setText(bundle.getString("ubicacion"));
        Log.i("Service","ubicacion " + bundle.getString("ubicacion"));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detalles, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()) {
            case R.id.crearRecordatorio:
                intent.setClass(DetallesRecordatorio.this, CrearRecordatorio.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.listarRecordatorios:
                intent.setClass(DetallesRecordatorio.this, ListarRecordatorios.class);
                startActivity(intent);
                finish();
                return true;

            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("Ha sido desarrollada para la asignatura Interacción Persona Ordenador II  " +
                        "de la Escuela Superior de Informática por: Álvaro Fernández Villa y " +
                        "Jaime Pérez Sánchez");
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Add your code for the button here.
                    }
                });
                alertDialog.setIcon(R.drawable.ic_info_outline_black_48dp);
                alertDialog.show();
                return true;
            case R.id.salir:
                finish();
                System.exit(0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void msg(String msg){
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }
}