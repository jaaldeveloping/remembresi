package ipo2.remembresi;


import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GestorRecordatorios {

    private String urlSetRecordatorio="http://remembresi.esy.es/RemembrESI/setRecordatorio.php";
    private String urlGetPRecordatorio="http://remembresi.esy.es/RemembrESI/getRecordatorio.php";
    private String urlDelGetPRecordatorio="http://remembresi.esy.es/RemembrESI/delRecordatorio.php";


    public String setRecordatorio(List<NameValuePair> nameValuePairs) {
        String respuesta = null;
        Log.i("Service","Update with " +respuesta);
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(urlSetRecordatorio);
        try {
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            respuesta = httpclient.execute(httppost, responseHandler);
            Log.i("Service","Update with " +respuesta);
        } catch (Exception e) {
        }
        return respuesta;
    }

    public String delRecordatorio(List<NameValuePair> nameValuePairs) {
        String respuesta = null;
        Log.i("Service","Update with " +respuesta);
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(urlDelGetPRecordatorio);
        try {
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            respuesta = httpclient.execute(httppost, responseHandler);
        } catch (Exception e) {
        }
        return respuesta;
    }

    public ArrayList<Recordatorio> getRecordatorio() {
        ArrayList<Recordatorio> recordatorios = new ArrayList<Recordatorio>();
        String respuesta = null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpget = new HttpPost(urlGetPRecordatorio);
            try {
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                respuesta = httpclient.execute(httpget, responseHandler);
            } catch (Exception e) {
            }
            JSONArray jsa = new JSONArray(respuesta);
            for (int i = 0; i < jsa.length(); i++) {

                JSONObject jso = jsa.getJSONObject(i);
                Recordatorio recordatorio = new Recordatorio();

                recordatorio.setId(jso.getInt("id"));
                recordatorio.setNombre(jso.getString("nombre"));
                recordatorio.setDescripcion(jso.getString("descripcion"));
                recordatorio.setCategoria(jso.getString("categoria"));
                recordatorio.setHora(jso.getString("hora"));
                recordatorio.setFecha(jso.getString("fecha"));
                recordatorio.setUbicacion(jso.getString("ubicacion"));

                recordatorios.add(recordatorio);

            }
        } catch (Exception e) {
        }
        return recordatorios;

    }
}