package ipo2.remembresi;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class AdaptadorListaRecordatorios extends ArrayAdapter<Recordatorio> {
    private Activity context;
    private ArrayList<Recordatorio> recordatorios;

    public AdaptadorListaRecordatorios(Activity context, ArrayList<Recordatorio> recordatorios) {
        super(context, R.layout.recordatorio, recordatorios);
        this.context = context;
        this.recordatorios = recordatorios;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater= context.getLayoutInflater();

        View v = inflater.inflate(R.layout.recordatorio, null);

        Log.i("Service","id " +recordatorios.get(position).getId());

        TextView lblTitulo= (TextView) v.findViewById(R.id.lblNombre);
        Log.i("Service","titulo " +recordatorios.get(position).getNombre());
        lblTitulo.setText(recordatorios.get(position).getNombre());

        TextView lblHora= (TextView) v.findViewById(R.id.lblHora);
        Log.i("Service","hora " +recordatorios.get(position).getHora());
        lblHora.setText(recordatorios.get(position).getHora());

        TextView lblFecha= (TextView) v.findViewById(R.id.lblFecha);
        Log.i("Service","fecha " +recordatorios.get(position).getFecha());
        lblFecha.setText(recordatorios.get(position).getFecha());

        ImageView ivCategoria= (ImageView) v.findViewById(R.id.ivCategoria);
        String categoria = recordatorios.get(position).getCategoria();
        switch (categoria) {
            case "Trabajo":
                ivCategoria.setImageResource(R.drawable.ic_clippy);
                break;
            case "Estudios":
                ivCategoria.setImageResource(R.drawable.ic_school);
                break;
            case "Familia":
                ivCategoria.setImageResource(R.drawable.ic_home);
                break;
            case "Amigos":
                ivCategoria.setImageResource(R.drawable.ic_group);
                break;
            case "Ocio":
                ivCategoria.setImageResource(R.drawable.ic_headphones);
                break;
            case "Otros":
                ivCategoria.setImageResource(R.drawable.ic_idea);
                break;
        }

        return v;
    }
}