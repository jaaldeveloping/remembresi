package ipo2.remembresi;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;


public class CrearRecordatorio extends AppCompatActivity {

    private EditText txtNombre;
    private EditText txtDescipcion;
    private Spinner spCategoria;
    private EditText txtHora;
    private EditText txtFecha;
    private EditText txtUbicacion;

    private Button btnAceptar;

    private ProgressDialog progressDialog;

    private GestorRecordatorios gestorRecordatorios;
    private String respuesta;
    private ArrayList<NameValuePair> pares;


    private class setRecordatorio extends AsyncTask<Integer, Integer, Integer> {
        private boolean subida=true;

        @Override
        protected Integer doInBackground(Integer... arg0) {
            Log.i("Service","Update with crear pares " +pares);
            respuesta = gestorRecordatorios.setRecordatorio(pares);
            if (respuesta!=null && respuesta.indexOf("ok")!=-1) {
                subida=true;
            }else{
                subida=false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            progressDialog.dismiss();
            if(subida){
                msg("Recordatorio guardado correctamente.");
                reseteo();
            }else{
                msg("Error a la hora de guardar el recordatorio.");
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crear_recordatorio);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Crear");
        setSupportActionBar(toolbar);

        gestorRecordatorios = new GestorRecordatorios();
        txtNombre=(EditText)findViewById(R.id.txtNombre);
        txtDescipcion=(EditText)findViewById(R.id.txtDescripcion);
        spCategoria=(Spinner) findViewById(R.id.spCategoria);
        txtHora=(EditText)findViewById(R.id.txtHora);
        txtFecha=(EditText)findViewById(R.id.txtFecha);
        txtUbicacion=(EditText)findViewById(R.id.txtUbicacion);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(this,R.array.categorias, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCategoria.setAdapter(adapter);

        btnAceptar=(Button)findViewById(R.id.btnAceptar);

        btnAceptar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //Antes de nada, volvemos a comprobar si hay internet
                if(getServicio()){
                    if(comprobar()){
                        pares = getPares();
                        progressDialog = ProgressDialog.show(CrearRecordatorio.this, null, "Subiendo...", true, false);
                        new setRecordatorio().execute();
                    }else{
                        msg("Todos los campos son obligatorios");
                    }
                }else{
                    msg("No dispone de conexion a internet en estos momentos.");
                    Intent intento = new Intent(CrearRecordatorio.this, Menu.class);
                    startActivity(intento);
                }
            }
        });

        txtHora.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TimePickerDialog mTimePicker = new TimePickerDialog(CrearRecordatorio.this, new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker timepicker, int selectedhour, int selectedminute) {
                        // TODO Auto-generated method stub
                        txtHora.setText(selectedhour+":"+selectedminute);
                    }
                },Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE),true);
                mTimePicker.setTitle("Seleccionar Hora");
                mTimePicker.show();
            }
        });

        txtFecha.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                DatePickerDialog mTimePicker =new DatePickerDialog(CrearRecordatorio.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int year, int month, int day) {
                        // TODO Auto-generated method stub
                        txtFecha.setText(day+"/"+month+"/"+year);
                    }
                },Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH),Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                mTimePicker.setTitle("Seleccionar Fecha");
                mTimePicker.show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu_main; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_crear, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()) {
            case R.id.listarRecordatorios:
                intent.setClass(CrearRecordatorio.this, ListarRecordatorios.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("Ha sido desarrollada para la asignatura Interacción Persona Ordenador II  " +
                        "de la Escuela Superior de Informática por: Álvaro Fernández Villa y " +
                        "Jaime Pérez Sánchez");
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Add your code for the button here.
                    }
                });
                alertDialog.setIcon(R.drawable.ic_info_outline_black_48dp);
                alertDialog.show();
                return true;
            case R.id.salir:
                finish();
                System.exit(0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void reseteo() {
        txtNombre.setText("");
        txtDescipcion.setText("");
        txtHora.setText("");
        txtFecha.setText("");
        txtUbicacion.setText("");
    }

    protected ArrayList<NameValuePair> getPares() {
        ArrayList<NameValuePair> pares=new ArrayList<NameValuePair>(6);
        pares.add(new BasicNameValuePair("nombre", txtNombre.getText().toString()));
        pares.add(new BasicNameValuePair("descripcion", txtDescipcion.getText().toString()));
        pares.add(new BasicNameValuePair("categoria", spCategoria.getSelectedItem().toString()));
        pares.add(new BasicNameValuePair("hora", txtHora.getText().toString()));
        pares.add(new BasicNameValuePair("fecha", txtFecha.getText().toString()));
        pares.add(new BasicNameValuePair("ubicacion", txtUbicacion.getText().toString()));
        return pares;
    }

    public AlertDialog CreateAlert(String title, String message) {
        AlertDialog alert = new AlertDialog.Builder(this).create();
        alert.setTitle(title);
        alert.setMessage(message);
        return alert;
    }

    public boolean comprobar() {
        if (txtNombre.getText().length() > 0 && txtDescipcion.getText().length() > 0
                && txtHora.getText().length() > 0 && txtFecha.getText().length() > 0
                && txtUbicacion.getText().length() > 0)
            return true; //Se puede
        else
            return false; //No se puede
    }

    public void msg(String msg){
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }

    public boolean getServicio() {
        ConnectivityManager cm = (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni != null && ni.isAvailable() && ni.isConnected())
            return true;
        else
            return false;
    }


}