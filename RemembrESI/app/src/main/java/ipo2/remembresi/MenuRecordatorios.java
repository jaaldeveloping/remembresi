package ipo2.remembresi;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;


public class MenuRecordatorios extends AppCompatActivity {
    private ImageButton ibCrear;
    private ImageButton ibListar;
    private ImageButton ibInfo;
    private ImageButton ibSalir;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_recordatorios);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Menu");
        setSupportActionBar(toolbar);

        ibCrear = (ImageButton)findViewById(R.id.ibCrear);
        ibListar = (ImageButton)findViewById(R.id.ibListar);
        ibInfo = (ImageButton) findViewById(R.id.ibInfo);
        ibSalir = (ImageButton)findViewById(R.id.ibSalir);

        ibCrear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(MenuRecordatorios.this, CrearRecordatorio.class);
                startActivity(i);
            }
        });

        ibListar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(MenuRecordatorios.this, ListarRecordatorios.class);
                startActivity(i);
            }
        });

        ibInfo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(MenuRecordatorios.this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("Ha sido desarrollada para la asignatura Interacción Persona Ordenador II  " +
                        "de la Escuela Superior de Informática por: Álvaro Fernández Villa y " +
                        "Jaime Pérez Sánchez");
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Add your code for the button here.
                    }
                });
                alertDialog.setIcon(R.drawable.ic_info_outline_black_48dp);
                alertDialog.show();
            }
        });

        ibSalir.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()) {
            case R.id.crearRecordatorio:
                intent.setClass(MenuRecordatorios.this, CrearRecordatorio.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.listarRecordatorios:
                intent.setClass(MenuRecordatorios.this, ListarRecordatorios.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("Ha sido desarrollada para la asignatura Interacción Persona Ordenador II  " +
                        "de la Escuela Superior de Informática por: Álvaro Fernández Villa y " +
                        "Jaime Pérez Sánchez");
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Add your code for the button here.
                    }
                });
                alertDialog.setIcon(R.drawable.ic_info_outline_black_48dp);
                alertDialog.show();
                return true;
            case R.id.salir:
                finish();
                System.exit(0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void msg(String msg){
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }
}