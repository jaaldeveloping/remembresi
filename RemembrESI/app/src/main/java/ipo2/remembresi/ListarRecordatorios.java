package ipo2.remembresi;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class ListarRecordatorios extends AppCompatActivity {
    private ListView lstRecordatorios;
    private ProgressDialog progressDialog;
    private ArrayList<Recordatorio> recordatorios = new ArrayList<>();
    private AdaptadorListaRecordatorios adaptadorListaRecordatorios;
    private GestorRecordatorios gestorRecordatorios;
    private ArrayList<NameValuePair> pares;
    private String respuesta;
    private int contactoSeleccionado;

    private class getRecordatorio extends AsyncTask<Integer, Integer, Integer> {

        @Override
        protected Integer doInBackground(Integer... arg0) {
            recordatorios = gestorRecordatorios.getRecordatorio();
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            progressDialog.dismiss();
            if(recordatorios != null ){
                adaptadorListaRecordatorios = new AdaptadorListaRecordatorios(ListarRecordatorios.this, recordatorios);
                lstRecordatorios.setAdapter(adaptadorListaRecordatorios);
                adaptadorListaRecordatorios.notifyDataSetChanged();
            }else{
                msg("No hay Recordatorios");
            }
        }
    }

    private class delRecordatorio extends AsyncTask<Integer, Integer, Integer> {
        private boolean borrado=true;
        @Override
        protected Integer doInBackground(Integer... arg0) {
            Log.i("Service","Update with crear pares " +pares);
            respuesta = gestorRecordatorios.delRecordatorio(pares);
            Log.i("Service","Update with crear respuesta" +respuesta);
            if (respuesta!=null && respuesta.indexOf("ok")!=-1) {
                borrado=true;
            }else{
                borrado=false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            progressDialog.dismiss();
            if(borrado){
                msg("Recordatorio borrado correctamente.");
            }else{
                msg("Error a la hora de borrar el recordatorio.");
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listar_recordatorios);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Listar");
        setSupportActionBar(toolbar);

        gestorRecordatorios = new GestorRecordatorios();

        lstRecordatorios = (ListView) findViewById(R.id.lstRecordatorios);
        registerForContextMenu(lstRecordatorios);

        adaptadorListaRecordatorios = new AdaptadorListaRecordatorios(ListarRecordatorios.this, recordatorios);
        lstRecordatorios.setAdapter(adaptadorListaRecordatorios);

        progressDialog = ProgressDialog.show(ListarRecordatorios.this, null, "Descargando...", true, false);
        new getRecordatorio().execute();

        lstRecordatorios.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int posicion, long id) {
                Intent i = new Intent(ListarRecordatorios.this, DetallesRecordatorio.class);
                Log.i("next23", "next"+ recordatorios.get(posicion).getNombre());
                i.putExtra("nombre", recordatorios.get(posicion).getNombre());
                i.putExtra("descripcion", recordatorios.get(posicion).getDescripcion());
                i.putExtra("categoria",recordatorios.get(posicion).getCategoria());
                i.putExtra("hora", recordatorios.get(posicion).getHora());
                i.putExtra("fecha", recordatorios.get(posicion).getFecha());
                i.putExtra("ubicacion",recordatorios.get(posicion).getUbicacion());
                startActivity(i);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_listar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()) {
            case R.id.crearRecordatorio:
                intent.setClass(ListarRecordatorios.this, CrearRecordatorio.class);
                startActivity(intent);
                finish();
                return true;

            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("Ha sido desarrollada para la asignatura Interacción Persona Ordenador II  " +
                        "de la Escuela Superior de Informática por: Álvaro Fernández Villa y " +
                        "Jaime Pérez Sánchez");
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Add your code for the button here.
                    }
                });
                alertDialog.setIcon(R.drawable.ic_info_outline_black_48dp);
                alertDialog.show();
                return true;
            case R.id.salir:
                finish();
                System.exit(0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_contextual, menu);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        contactoSeleccionado = info.position;

    }

    public boolean onContextItemSelected (MenuItem item) {
        switch (item.getItemId()) {
            case R.id.verDetalles:
                Intent i = new Intent(this, DetallesRecordatorio.class);
                Log.i("next23", "next"+ recordatorios.get(contactoSeleccionado).getNombre());
                i.putExtra("nombre", recordatorios.get(contactoSeleccionado).getNombre());
                i.putExtra("descripcion", recordatorios.get(contactoSeleccionado).getDescripcion());
                i.putExtra("categoria",recordatorios.get(contactoSeleccionado).getCategoria());
                i.putExtra("hora", recordatorios.get(contactoSeleccionado).getHora());
                i.putExtra("fecha", recordatorios.get(contactoSeleccionado).getFecha());
                i.putExtra("ubicacion",recordatorios.get(contactoSeleccionado).getUbicacion());
                startActivity(i);
                break;
            case R.id.borrarRecordatorio:
                pares = getPares();
                Log.i("Service","Update with " +pares);
                progressDialog = ProgressDialog.show(ListarRecordatorios.this, null, "Borrando...", true, false);
                new delRecordatorio().execute();
        }
        return true;
    }

    protected ArrayList<NameValuePair> getPares() {
        ArrayList<NameValuePair> pares=new ArrayList<NameValuePair>(1);
        pares.add(new BasicNameValuePair("id", Integer.toString(recordatorios.get(contactoSeleccionado).getId())));
        return pares;
    }

    public void msg(String msg){
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }

}
